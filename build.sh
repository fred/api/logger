if [[ x$1 = xclean ]]
then
    rm -rf ./c++/ ./python/
else
    mkdir c++ python
    /usr/local/bin/protoc --cpp_out ./c++ ./fred_api/logger/service_logger_grpc.proto
    /usr/local/bin/protoc --python_out ./python ./fred_api/logger/service_logger_grpc.proto
fi
