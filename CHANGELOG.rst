ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

4.2.0 (2023-10-05)
------------------

* Add ``properties`` to ListLogEntriesRequest

4.1.3 (2022-03-31)
------------------

* Replace ``setup_requires`` with ``pyproject.toml``.

4.1.2 (2022-03-04)
------------------

* Update CMake build scripts (use ``pkg-config``)
* Fix changelog

4.1.1 (2022-02-10)
------------------

* Update CMake build scripts

4.1.0 (2022-02-08)
------------------

* Changed ``LogEntryInfo``

  * add ``log_entry_id``
  * add ``session_id``

4.0.2 (2022-03-31)
------------------

* Add ``setup.py`` to manifest.

4.0.1 (2022-03-30)
------------------

* Replace ``setup_requires`` with ``pyproject.toml``.

4.0.0 (2021-08-04)
------------------

* Drop Fred.Logger.Diagnostics.
* Convert README.md to RST.
* Update dependencies in Python.
* Rename ``LOGGER_API_VERSION`` to ``API_LOGGER_VERSION``.

3.2.3 (2022-03-31)
------------------

* Add ``setup.py`` to manifest.

3.2.2 (2022-03-30)
------------------

* Replace ``setup_requires`` with ``pyproject.toml``.

3.2.1 (2020-11-05)
------------------

* Fix changelog.

3.2.0 (2020-06-11)
------------------

* Add procedures to register new services, log entry types, results and reference types.

3.1.0 (2020-06-09)
------------------

* Changed ``LogEntryInfo``

  * Separate input/output log entry properties introduced
  * Undifferentiated log entry properties deprecated (will be removed in the next version)

3.0.2 (2020-03-05)
------------------

Bug fixes

* Fix Python package versioning

3.0.1 (2020-02-04)
------------------

Bug fixes

* Pin grpcio-tools version in setup dependencies

3.0.0 (2019-09-17)
------------------

* Changed ``ListLogEntriesRequest``

  * Rename ``from`` and ``to`` attributes to ``timestamp_from`` and ``timestamp_to``
  * Rename ``log_entry_type`` attribute to``log_entry_types``

* Add exceptions to ``ListLogEntriesReply``
* Add diagnostic API

2.0.0 (2019-06-25)
------------------

* Add ``SearchLoggerHistory`` API
* Split common types
* Rename ``username`` attribute

1.0.0 (2019-04-15)
------------------

* Initial ``Logger`` API
