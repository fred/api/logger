cmake_minimum_required(VERSION 3.5)

set(API_LOGGER_VERSION "4.2.0")
message(STATUS "API_LOGGER_VERSION: ${API_LOGGER_VERSION}")

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/include.cmake")
set(PROTOS_DIR ${CMAKE_CURRENT_LIST_DIR})
set(PROTOBUF_IMPORT_DIRS ${PROTOS_DIR})
set(FRED_LOGGER_GENERATE_INTO ${CMAKE_CURRENT_BINARY_DIR})

set(FRED_LOGGER_PROTO_FILES
    ${PROTOS_DIR}/fred_api/logger/log_entry_common_types.proto
    ${PROTOS_DIR}/fred_api/logger/service_search_logger_history_grpc.proto
    ${PROTOS_DIR}/fred_api/logger/service_logger_grpc.proto)
set(FRED_LOGGER_GRPC_FILES
    ${PROTOS_DIR}/fred_api/logger/service_search_logger_history_grpc.proto
    ${PROTOS_DIR}/fred_api/logger/service_logger_grpc.proto)

find_package(ProtobufCpp REQUIRED)
find_package(GrpcCpp REQUIRED)

protobuf_generate_cpp(FRED_LOGGER_PROTO_SRCS FRED_LOGGER_PROTO_HDRS ${FRED_LOGGER_GENERATE_INTO} ${FRED_LOGGER_PROTO_FILES})
grpc_generate_cpp(FRED_LOGGER_GRPC_SRCS FRED_LOGGER_GRPC_HDRS ${FRED_LOGGER_GENERATE_INTO} ${FRED_LOGGER_GRPC_FILES})
