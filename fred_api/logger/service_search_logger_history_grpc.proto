syntax = "proto3";

import "fred_api/logger/log_entry_common_types.proto";

import "google/protobuf/timestamp.proto";

package Fred.Logger.Api;

message LogEntryInfo
{
    google.protobuf.Timestamp time_begin = 1;
    google.protobuf.Timestamp time_end = 2;
    SourceIp source_ip = 3;
    LogEntryService service = 4;
    LogEntryType log_entry_type = 5; // valid values could be obtained by calling get_log_entry_types
    oneof OptionalResultCode
    {
        uint64 result_code = 6;
    }
    oneof OptionalResultName
    {
        string result_name = 7;
    }
    LogEntryContent raw_request = 8;
    LogEntryContent raw_response = 9;
    Username username = 10;
    UserId user_id = 11;
    map<string, LogEntryPropertyValues> properties = 12; // deprecated, use input_properties and output_properties instead
    map<string, ObjectReferenceValues> references = 13; // key is ObjectReferenceType (cannot be non-primitive type)
    map<string, LogEntryPropertyValues> input_properties = 14; // key is log entry property type (cannot be non-primitive type)
    map<string, LogEntryPropertyValues> output_properties = 15; // key is log entry property type (cannot be non-primitive type)
    LogEntryId log_entry_id = 16;
    SessionId session_id = 17;
}

message ListLogEntriesRequest
{
    oneof User
    {
        Username username = 1;
        UserId user_id = 2;
    }
    google.protobuf.Timestamp timestamp_from = 3; // request log entries with time_begin from
    google.protobuf.Timestamp timestamp_to = 4;   // interval [timestamp_from, timestamp_to)
    LogEntryService service = 5;
    repeated LogEntryType log_entry_types = 6; // valid values could be obtained by calling get_log_entry_types
    ObjectReference reference = 7;
    uint64 count_limit = 8;
    // Filter entries by properties.
    // If more properties with the same name but different values is provided, at least one of these values must match.
    // Properties with different names must all match.
    repeated LogEntryProperty properties = 9;
}

message ListLogEntriesReply
{
    oneof DataOrException
    {
        Data data = 1;
        Exception exception = 2;
    }

    message Data
    {
        repeated LogEntryId log_entry_ids = 1; // sorted by request.time_begin in descending order
    }

    message Exception
    {
        oneof Reasons
        {
            LogEntryServiceNotFound log_entry_service_not_found = 1;
            InvalidTimestampFrom invalid_timestamp_from = 2;
            InvalidTimestampTo invalid_timestamp_to = 3;
            InvalidTimestampRange invalid_timestamp_range = 4;
            InvalidCountLimit invalid_count_limit = 5;
        }
        message LogEntryServiceNotFound { }
        message InvalidTimestampFrom { }
        message InvalidTimestampTo { }
        message InvalidTimestampRange { }
        message InvalidCountLimit { }
    }
}

message GetLogEntryInfoRequest
{
    LogEntryId log_entry_id = 1;
}

message GetLogEntryInfoReply
{
    oneof DataOrException
    {
        Data data = 1;
        Exception exception = 2;
    }

    message Data
    {
        LogEntryInfo log_entry_info = 1;
    }

    message Exception
    {
        oneof Reasons
        {
            LogEntryDoesNotExist log_entry_does_not_exist = 1;
        }
    }
}

service SearchLoggerHistory
{
    // Returns list of log entry ids by user name, time, service, type of log entry and reference
    rpc list_log_entries(ListLogEntriesRequest) returns (ListLogEntriesReply) {}
    // Returns detailed data about log entry by id
    rpc get_log_entry_info(GetLogEntryInfoRequest) returns (GetLogEntryInfoReply) {}
}
